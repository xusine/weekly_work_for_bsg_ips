#include"verilated.h"
#include"bsg_wallace_tree/Vbsg_wallace_tree.h"

#include<stdlib.h>
#include<stdio.h>
#include<time.h>

int main(int argc, char **argv){
    Verilated::commandArgs(argc, argv);
    Vbsg_wallace_tree *dut = new Vbsg_wallace_tree;

    srand(time(NULL));

    int64_t value[PORT_NUM];

    for(int j = 0; j < 20; ++j) {
        int64_t sums = 0;
        for(int i = 0; i < PORT_NUM; ++i){
            value[i] = rand();
            sums += int64_t(value[i]);
            dut->ports_i[i] = value[i];
        }

        dut->eval();

        int64_t res = dut->output_o;

        printf("in SV: %lld,in C++: %lld, difference: %lld\n",res, sums, res- sums);
    }
    return 0;
}