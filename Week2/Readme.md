## Week2
Mar. 11 - Mar.17
By Shanqing Lin, Fudan University

#### CSA String Multiplier (CSA_String_MUL)

1. Additional comments are complemented.

2. A C++ testbench for Verilator is written.

3. The multiplier is divided into two submodules for diverse output size.(full version and half version)

#### Wallace Tree (Wallace_Tree)

1. A python script is coded to generate arbitrary Wallace tree with 3-2 CSA

2. A C++ testbench for Verilator is written.

#### Multiplier Using 4-2-CSA-based Wallace Tree

1. A 4-2-CSA-based Wallace tree is built.

2. The CSAs String in multiplier is replaced with 4-2-CSA-based Wallace tree.

3. A C++ testbench for Verilator is written.

