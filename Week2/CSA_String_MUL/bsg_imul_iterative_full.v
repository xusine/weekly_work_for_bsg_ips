/*************************************************************
==================================================
bsg_imul_iterative_full.v
3/14/2019, sqlin16@fudan.edu.cn
===================================================

An 32 bits integer interative multiplier, capable of singed & unsigned division,
with configurable stide for each iteration.

States:

eIDNE:          Ready for input. 
eNEG_A,eNEG_B:  Calculate A/B absolute value
eCAL:           Add the partial product of result using CSA.
eNEG_R_L/H:     Determine the sign of the high/low part of result
eDONE:          Output the result


Parameters:

width_p:        Length of opreands
iter_step_p:    Stride of each iteration of adding.(How many partial products are added in one iteration)


************************************************************/
module bsg_imul_iterative_full #(
  parameter width_p = 32
  ,parameter iter_step_p = 4
)
(
  input clk_i
  ,input rst_i
  ,input reset_when_done_i
  // shakehand signal
  ,input op_v_i
  ,output ready_o
  // operands
  ,input [width_p-1:0] opA_i
  ,input opA_is_signed_i
  ,input [width_p-1:0] opB_i
  ,input opB_is_signed_i
  // result
  ,output [2*width_p-1:0] result_o 
  ,output result_v_o
  
);

  localparam integer iter_count_lp = (width_p - 1)/iter_step_p + 1; // calculate how many iterations shoubd be done.
  localparam cal_state_length_lp = `BSG_SAFE_CLOG2(iter_count_lp);  // calculate the length of counter used for eCAL state.

  localparam actual_width_lp = iter_count_lp * iter_step_p;         // to align the width_p, which is equal to width_p providing width_p % iter_step_p == 0.
  

  wire self_is_ready = ready_o & op_v_i; 
  // used for reset registers. combining the rst_i and reset_when_done_i
  wire reset_internal = rst_i | (reset_when_done_i & current_state_r == eDONE); 
  // counter used for jumping out of eCAL
  logic [cal_state_length_lp-1:0] cal_state_counter_r;
  // counter update
  always_ff @(posedge clk_i) begin
    if(reset_internal) cal_state_counter_r <= '0;
    else cal_state_counter_r <= (current_state_r == eCAL) ? cal_state_counter_r + 1 : cal_state_counter_r;
  end

  // FSM
  //typedef enum [2:0] {eIDNE, eNEG_A, eNEG_B, eCAL, eNEG_R_LOW, eNEG_R_HIGH, eDONE} state_e;
  typedef enum [2:0] {eIDNE, eNEG_A, eNEG_B, eCAL, eNEG_R_LOW, eNEG_R_HIGH, eDONE} state_e;
  state_e current_state_r, current_state_n;
  always_comb begin
    unique case(current_state_r)
      eIDNE : begin
        if(self_is_ready) 
            current_state_n = eNEG_A; 
        else 
          current_state_n = eIDNE;
      end
      eNEG_A : begin
        current_state_n = eNEG_B;
      end
      eNEG_B : begin
        current_state_n = eCAL;
      end
      eCAL : begin
        if(cal_state_counter_r == iter_count_lp - iter_count_lp'(1))
            current_state_n = eNEG_R_LOW;
        else
            current_state_n = eCAL;
      end
      eNEG_R_LOW : begin
        current_state_n = eNEG_R_HIGH;
      end
      eNEG_R_HIGH : begin
        current_state_n = eDONE;
      end
      eDONE:  current_state_n = eDONE;
      default : current_state_n = eIDNE;
    endcase
  end

  // update current_state_r
  assign ready_o = current_state_r == eIDNE;

  always_ff @(posedge clk_i) begin
    if(reset_internal) begin
      current_state_r <= eIDNE;
    end
    else begin
      current_state_r <= current_state_n;
    end
  end
  // registers to store operands
  logic [actual_width_lp - 1:0] opA_r; 
  logic [actual_width_lp - 1:0] opB_r;
  logic opA_neg_r; // used to record that opA is negative.
  logic opB_neg_r;
  // update input registers (opA, opB, opA_neg, opB_neg)
  always_ff @(posedge clk_i) begin
    if(reset_internal) begin
      opA_r <= '0;
      opB_r <= '0;
      opA_neg_r <= 1'b0;
      opB_neg_r <= 1'b0;
    end
    else begin
      unique case(current_state_r)  // latch up!
        eIDNE: begin
          opA_r <= opA_i;
          opB_r <= opB_i;
          opA_neg_r <= opA_i[actual_width_lp-1] & opA_is_signed_i;
          opB_neg_r <= opB_i[actual_width_lp-1] & opB_is_signed_i;
        end
        eNEG_A: begin
          if(opA_neg_r) opA_r <= cpa_opt[actual_width_lp-1:0]; else opA_r <= opA_r;
        end
        eNEG_B: begin
          if(opB_neg_r) opB_r <= cpa_opt[actual_width_lp-1:0]; else opB_r <= opB_r;
        end
        eCAL : begin
          opB_r <= (opB_r >> iter_step_p);
        end
        default : begin
        end
      endcase
    end
  end

  // the input of carry propagate adder
  logic [actual_width_lp:0] cpa_opA;
  logic [actual_width_lp:0] cpa_opB;

  wire [actual_width_lp+1:0] cpa_opt = cpa_opA + cpa_opB; //carry propagate adder output

  // configure cpa operands.
  always_comb begin
    unique case(current_state_r) 
      eIDNE: begin
        cpa_opA = '0;
        cpa_opB = '0;
      end
      eNEG_A: begin // ~A + 1
        cpa_opA = {1'b0,~opA_r};
        cpa_opB = (actual_width_lp+1)'(1);
      end
      eNEG_B: begin // ~B + 1
        cpa_opA = {1'b0, ~opB_r};
        cpa_opB = (actual_width_lp+1)'(1);
      end
      /**
        csa_sum and csa_carry are the output of last carry save adder.
        CSAs need a CPA to generate final result.
      **/
      eCAL : begin 
        cpa_opA = csa_sum;
        cpa_opB = csa_carry;
      end
      eNEG_R_LOW: begin
        cpa_opA = {1'b0, ~result_r[actual_width_lp-1:0]};
        cpa_opB = (actual_width_lp+1)'(1);
      end
      eNEG_R_HIGH: begin
        cpa_opA = {1'b0, ~result_r[actual_width_lp+:actual_width_lp]};
        cpa_opB = {{actual_width_lp{1'b0}}, carry_of_neg_res_r};
      end
      default: begin
        cpa_opA = '0;
        cpa_opB = '0;
      end
    endcase
  end

  // CSA network output.
  logic [actual_width_lp:0] csa_sum;    // sum of last CSA
  logic [actual_width_lp:0] csa_carry;  // carry of last CSA
  logic [iter_step_p-1:0] csa_out;      // the bits of final result produced in current iteration.
  
  wire result_is_negative = opA_neg_r ^ opB_neg_r;

  reg [2*actual_width_lp-1:0] result_r;
  reg [actual_width_lp:0] result_remnant_r; // the bits need to be put into next turn iteration.
  reg carry_of_neg_res_r; // the carry by making lower bit negative.

/**************************************************
width_p = 4, iter_step_p = 2
                A3    A2    A1    A0
                B3    B2    B1    B0
First Iter=========================================
                A3B0  A2B0 | A1B0  A0B0
          A3B1  A2B1  A1B1 | A0B1
-----------------------------------------------------
 result_remnant[cycle_1]   |  csa_out[cycle_1]
Second Iter0============================================
               result_remnant
      A3B2  A2B2 | A1B2  A0B2
A3B3  A2B3  A1B3 | A0B3          
-----------------------------------------------------
 result_remnant[cycle2]  |   csa_out[cycle_2]

The result is {result_remnant[ccyle2],csa_out[cycle2],csa_out[cycle1]}

To achieve a multi-operand addition, a series of full adders are implemented like:
                    ______                   _____       _____                          ___    
result_remnant----->|  C  | sums[0][1+:]--->|  C  | ... |  C  | csa_sum[final]   ----> | C | low bit   --> csa_out[3]
opA&opB[0]--------->|  S  | carry[0]------->|  S  | ... |  S  | csa_carry[final] ----> | P | high bits --> result_remnant
opA&opB[1] << 1 --->|  A  | opA&opB[2]----->|  A  | ... |  A  |                        | A |
****************************************************/

  generate begin: ADDER_BLOCK
    if(iter_step_p == 1) begin: NO_CSA
      assign csa_sum = {1'b0,opA_r & {actual_width_lp{opB_r[0]}}};
      assign csa_carry = result_remnant_r;
    end
    else begin: CSA_GROUP
      wire [actual_width_lp:0] sums [iter_step_p-2:0];
      wire [actual_width_lp:0] carrys [iter_step_p-2:0];
      // first CSA, which adds remnant, opA_r and opA_r << 1 together.
      bsg_full_adder #(.width_p(actual_width_lp+1))
      first_csa(.opA_i(result_remnant_r)
        ,.opB_i({1'b0, opA_r & {actual_width_lp{opB_r[0]}}})
        ,.opC_i({opA_r & {actual_width_lp{opB_r[1]}}, 1'b0})
        ,.res_o(sums[0])
        ,.car_o(carrys[0])
      );
      assign csa_out[0] = sums[0][0];
      for(genvar i = 1; i < iter_step_p - 1;i++) begin : OTHER_CSA
          bsg_full_adder #(.width_p(actual_width_lp + 1))
          csa(.opA_i({1'b0, sums[i-1][1+:actual_width_lp]})
            ,.opB_i(carrys[i-1])
            ,.opC_i({opA_r & {actual_width_lp{opB_r[i+1]}}, 1'b0})
            ,.res_o(sums[i])
            ,.car_o(carrys[i])
          );
          assign csa_out[i] = sums[i][0];
      end
      assign csa_sum = {1'b0, sums[iter_step_p-2][actual_width_lp:1]};
      assign csa_carry = carrys[iter_step_p-2];
    end
  end
  endgenerate

  assign csa_out[iter_step_p-1] = cpa_opt[0];

  // update of result_r, result_remnant_r and carry_of_neg_res_r.
  always_ff @(posedge clk_i) begin
    if(reset_internal) begin
      result_r <= '0;
      result_remnant_r <= '0;
      carry_of_neg_res_r <= 1'b0;
      result_r <= '0;
    end
    else begin
        unique case(current_state_r) 
          eCAL: begin
            result_remnant_r <= {1'b0,cpa_opt[1+:actual_width_lp]};
            result_r[actual_width_lp-1:0] <= {csa_out,result_r[actual_width_lp-1:iter_step_p]};
            carry_of_neg_res_r <= 1'b0;
            if(cal_state_counter_r == iter_count_lp - iter_count_lp'(1)) begin
              result_r <= {cpa_opt[1+:actual_width_lp],{csa_out,result_r[actual_width_lp-1:iter_step_p]}};
            end
          end
          eNEG_R_LOW: begin // making low part negative
            result_remnant_r <= '0;
            if(result_is_negative) begin
              result_r[0+:actual_width_lp] <= cpa_opt[actual_width_lp-1:0];
              carry_of_neg_res_r <= cpa_opt[actual_width_lp];
            end
            else begin
              carry_of_neg_res_r <= 1'b0;
            end
          end
          eNEG_R_HIGH: begin // for high part
            result_remnant_r <= '0;
            if(result_is_negative) begin
              result_r[actual_width_lp+:actual_width_lp] <= cpa_opt[actual_width_lp-1:0];
              carry_of_neg_res_r <= cpa_opt[actual_width_lp];
            end
            else begin
              carry_of_neg_res_r <= 1'b0;
            end
          end
          default: begin
            result_remnant_r <= '0;
            carry_of_neg_res_r <= 1'b0;
          end
        endcase
    end
  end
  
  assign result_v_o = current_state_r == eDONE;
  assign result_o = result_r[2*width_p-1:0];
endmodule