/***************** TEST RATIONALE *************************
1. STATEB SPACE
  25 random inputs for each unsigned and signed operands.
2. PARAMETERIZATION
  At present iter_step = 4 and width_p = 32.
*********************************************************/
module test_bsg;
  localparam width_lp = 16;
  logic clk_li, rst_li, reset_when_done_li, op_v_li, ready_lo;
  logic opA_is_signed_li, opB_is_signed_li;

  logic unsigned [width_lp-1:0] opA_li;
  logic unsigned [width_lp-1:0] opB_li;

  logic unsigned [2*width_lp-1:0] result_o;
  logic result_v_o;
  logic [2*width_lp-1:0] x;

  bsg_imul_config_iterative #(.width_p(width_lp)
    ,.iter_step_p(4)
  )
  dut(.clk_i(clk_li)
    ,.rst_i(rst_li)
    ,.reset_when_done_i(reset_when_done_li)
    ,.op_v_i(op_v_li)
    ,.ready_o(ready_lo)
    ,.opA_i(opA_li)
    ,.opA_is_signed_i(opA_is_signed_li)
    ,.opB_i(opB_li)
    ,.opB_is_signed_i(opB_is_signed_li)

    ,.result_o(result_o)
    ,.result_v_o(result_v_o)
  );

  bsg_nonsynth_clock_gen #(.cycle_time_p(100))
  clk_gen(.o(clk_li));

  bsg_nonsynth_reset_gen #(.num_clocks_p(1)
    ,.reset_cycles_lo_p(1)
    ,.reset_cycles_hi_p(3)
  )
  rst_gen(.clk_i(clk_li)
    ,.async_reset_o(rst_li)
  );

  logic [31:0] i;
  
  assign x = opA_li * opB_li;
  initial begin
    
  end
  always_ff @(posedge clk_li) begin
    if(rst_li) begin
        $display("========================================");
        i <= 0;
        // reset
        reset_when_done_li <= 0;
        opA_is_signed_li <= 0;
        opB_is_signed_li <= 0;
    
        opA_li <= $random;
        opB_li <= $random;
    
        $display("========================================");
        $display("opA = %b, opB = %b",opA_li, opB_li);
    
        op_v_li <= 1;    
       
    end
    else if(result_v_o) begin
      $display("Done at %d",i);
      $display("opA=%b, opB=%b",opA_li,opB_li);
      $display("Running Result:%d, %x",result_o,result_o);
      $display("Actual Result:%d, %x",x,x);
      $display("%b\n%b",result_o,x);
      $finish;
    end
    else begin
        i <= i + 1;
    end
  end
endmodule