/*************************************************************
==================================================
bsg_imul_iterative.v
3/15/2019, sqlin16@fudan.edu.cn
===================================================

An 32 bits integer interative multiplier, capable of singed & unsigned division,
with configurable stide for each iteration.
This is a wrapper of full version multiplier and half version multiplier.
Parameters:
width_p:        Length of opreands
iter_step_p:    Stride of each iteration of adding.(How many partial products are added in one iteration)
full_output_p:  A 64-bit-length result is output.

************************************************************/
`include "bsg_imul_iterative_full.v"
`include "bsg_imul_iterative_half.v"

module bsg_imul_iterative #(
  parameter width_p = 32
  ,parameter iter_step_p = 4
  ,parameter full_output_p = 1
)
(
  input clk_i
  ,input rst_i
  ,input reset_when_done_i
  // shakehand signal
  ,input op_v_i
  ,output ready_o
  // operands
  ,input [width_p-1:0] opA_i
  ,input opA_is_signed_i
  ,input [width_p-1:0] opB_i
  ,input opB_is_signed_i
  // result
  ,output [(1+full_output_p)*width_p-1:0] result_o 
  ,output result_v_o
);

generate
  if(full_output_p == 1)
    bsg_imul_iterative_full #(
      .width_p(width_p)
      ,.iter_step_p(iter_step_p)
    ) 
    imul(
      .clk_i(clk_i)
      ,.rst_i(rst_i)
      ,.reset_when_done_i(reset_when_done_i)
      ,.op_v_i(op_v_i)
      ,.ready_o(ready_o)
      ,.opA_i(opA_i)
      ,.opA_is_signed_i(opA_is_signed_i)
      ,.opB_i(opB_i)
      ,.opB_is_signed_i(opB_is_signed_i)
      ,.result_o(result_o)
      ,.result_v_o(result_v_o)
    );
  else
    bsg_imul_iterative_half #(
      .width_p(width_p)
      ,.iter_step_p(iter_step_p)
    ) 
    imul(
      .clk_i(clk_i)
      ,.rst_i(rst_i)
      ,.reset_when_done_i(reset_when_done_i)
      ,.op_v_i(op_v_i)
      ,.ready_o(ready_o)
      ,.opA_i(opA_i)
      ,.opA_is_signed_i(opA_is_signed_i)
      ,.opB_i(opB_i)
      ,.opB_is_signed_i(opB_is_signed_i)
      ,.result_o(result_o)
      ,.result_v_o(result_v_o)
    );
endgenerate
endmodule