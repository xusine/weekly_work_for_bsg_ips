/***************** TEST RATIONALE *************************
1. STATEB SPACE
  Random input for unsigned and signed operands.
2. PARAMETERIZATION
  width_p = 32, iter_step_p = 4
*********************************************************/

#include "verilated.h"
#include "bsg_imul_iterative/Vbsg_imul_iterative.h"
//#include "bsg_imul_iterative_half/Vbsg_imul_iterative_half.h"

#include<stdio.h>
#include<stdlib.h>
#include<time.h>
#include<inttypes.h>

int main(int argc, char** argv){
    Verilated::commandArgs(argc,argv);
    srand(time(NULL));
    Vbsg_imul_iterative *dut = new Vbsg_imul_iterative;
    // initial begin
        dut->clk_i = 0;
        dut->rst_i = 0;
        dut->reset_when_done_i = 1;
        dut->op_v_i = 0;
        dut->opA_i = 0;
        dut->opA_is_signed_i = 0;
        dut->opB_i = 0;
        dut->opB_is_signed_i = 0;
    // end
    dut->eval();
    // Reset
    dut->clk_i = 1;
    dut->rst_i = 1;
    dut->eval();

    dut->clk_i = 0;
    dut->rst_i = 0;
    dut->eval();
    // unsigned value

    printf("============Testing unsigned number!===============\n");

    for (int i = 0; i < 10; ++i) {

        unsigned int opA = rand();
        unsigned int opB = rand();

        dut->opA_i = opA;
        dut->opB_i = opB;
        dut->op_v_i = 1;
        int turn = 0;
        while(dut->result_v_o == 0){
            dut->clk_i = 1;
            dut->eval();
            dut->clk_i = 0;
            dut->eval();
            ++turn;
        }

        unsigned int res = dut->result_o;
        unsigned int cpp_res = opA*opB;

        printf("Afer %u, %u * %u, Output: %u, In C++: %u, difference: %d\n",turn,opA,opB,res, cpp_res, res - cpp_res);
        dut->clk_i = 1;
        dut->eval();
        dut->clk_i = 0;
        dut->eval();
    }

    dut->opA_is_signed_i = 1;
    dut->opB_is_signed_i = 1;

    printf("============Testing signed number!===============\n");

    for (int i = 0; i < 10; ++i) {

        int opA = rand();
        int opB = rand();

        dut->opA_i = opA;
        dut->opB_i = opB;
        dut->op_v_i = 1;
        int turn = 0;
        while(dut->result_v_o == 0){
            dut->clk_i = 1;
            dut->eval();
            dut->clk_i = 0;
            dut->eval();
            ++turn;
        }

        int res = dut->result_o;
        int cpp_res = opA*opB;

        printf("Afer %d, %d * %d, Output: %d, In C++: %d, difference: %d\n",turn,opA, opB,res , cpp_res, res - cpp_res);
        dut->clk_i = 1;
        dut->eval();
        dut->clk_i = 0;
        dut->eval();
    }

    return 0;
}
