/**
Shanqing Lin 3/3/2019

busy_beaver

This is a simple and famous Turing machine used for disprove halting problem.
A 3-state busy beaver is defined with such a control logic:

Value in Tape |  A  |  B  |  C  | 
----------------------------------
       0      | 1RB | 0RC | 1LC |
       1      | 1RH | 1RB | 1LA |

The term in table presents the action:
    1/0     : Value in current position in tape will be changed to 1 or 0.
    R/L     : Tape should move right or left.
    A/B/C/H : Next state

H means "halt" and the beaver will "rest" until reseted.

The tape is initialized to all zero.

***/

module busy_beaver
(
  input clk_i
 ,input rst_i
 ,input r_value_i       // value read from tape

 ,output r_v_o         // read valid
 ,output [3:0] r_addr_o// read address

 ,output w_v_o
 ,output w_data_o      // position put to the tape
 ,output [3:0] w_addr_o // position of tape
);

  typedef enum [1:0] {eA, eB, eC, eHalt} state_e;

  state_e state_n, state_r;

  reg [3:0] pos_r;
  logic [3:0] pos_fix;

  always_comb begin : FSM
    unique case(state_r) 
      eA : begin
        state_n = r_value_i ? eHalt : eB;
        pos_fix = 4'b0001;
        w_data_o = 1;
      end
      eB : begin
        state_n = r_value_i ? eB : eC;
        pos_fix = 4'b0001;
        w_data_o = r_value_i;
      end
      eC : begin
        state_n = r_value_i ? eA : eC;
        pos_fix = '1;
        w_data_o = 1;
      end
      eHalt : begin
          state_n = eHalt;
          pos_fix = '0;
          w_data_o = r_value_i;
      end
      default : begin // error!
          state_n = eHalt;
          pos_fix = 'X;
          w_data_o = 1'bX;
      end
    endcase
  end

  wire [3:0] w_addr_n = pos_r + pos_fix;

  always_ff @(posedge clk_i) begin
    if(rst_i) begin
      state_r <= eA;
      pos_r <= 4'b1000;
      $display("Initialize at state %s. \n", state_r.name());
    end
    else begin
      state_r <= state_n;
      pos_r <= w_addr_n;
      if (state_r != eHalt)
        $display("Switch to state: %s \n", state_r.name());
    end
  end


  assign w_addr_o = pos_r;
  assign r_addr_o = w_addr_n;
  assign r_v_o = state_r != eHalt;
  assign w_v_o = r_v_o;
  
    
endmodule
