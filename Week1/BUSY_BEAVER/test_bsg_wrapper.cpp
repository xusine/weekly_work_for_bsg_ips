#include<stdio.h>
#include "verilated.h"
#include"test_bsg/Vtest_bsg.h"

vluint64_t main_time = 0; 

double sc_time_stamp(){
    return main_time;
}

int main(int argc, char **argv){
    Verilated::commandArgs(argc, argv);

    Vtest_bsg *tb = new Vtest_bsg;
    while(!Verilated::gotFinish()){
        tb->eval();
        ++main_time;
    }
    exit(EXIT_SUCCESS);
    
}