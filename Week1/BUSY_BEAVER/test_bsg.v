/*********************  TEST RATIONALE *******************************
1. STATE SPACE
    In fact there is no need to specify the input, what only to do is to iterate the busy beaver until halting.
2. PARAMETERIZATION
    No parameter needs testing.
*********************************************************************/

module test_bsg;

  wire clk_li;
  wire rst_li;

  wire r_value_li;
  wire r_v_lo;
  wire [3:0] r_addr_lo;

  wire w_v_lo;
  wire w_data_lo;
  wire [3:0] w_addr_lo;

  bsg_nonsynth_clock_gen #(.cycle_time_p(80)) 
  clk_gen(.o(clk_li));

  bsg_nonsynth_reset_gen #(.num_clocks_p(1)
     ,.reset_cycles_lo_p(1)
     ,.reset_cycles_hi_p(5)
  )
  rst_gen(.clk_i(clk_li)
     ,.async_reset_o(rst_li)
  );

  bsg_mem_1r1w_sync #(.width_p(1)
     ,.els_p(16)
  ) 
  tape(.clk_i(clk_li)
     ,.reset_i(rst_li)
     ,.w_v_i(w_v_lo)
     ,.w_addr_i(w_addr_lo)
     ,.w_data_i(w_data_lo)
     ,.r_v_i(r_v_lo)
     ,.r_addr_i(r_addr_lo)
     ,.r_data_o(r_value_li)
  );

  busy_beaver beaver(.clk_i(clk_li)
     ,.rst_i(rst_li)

     ,.r_value_i(r_value_li)
     ,.r_v_o(r_v_lo)
     ,.r_addr_o(r_addr_lo)
     
     ,.w_v_o(w_v_lo)
     ,.w_data_o(w_data_lo)
     ,.w_addr_o(w_addr_lo)
  );
  integer i;
  initial i = 0;
  always_ff @(posedge clk_li) begin
      if(r_v_lo) begin
        $display("====================== Round %d ====================\n",i);
        $display("Current position: %d\n", w_addr_lo);
        $display("Current put down: %d\n",w_data_lo);
        $display("Next position: %d\n",r_addr_lo);
        i <= i + 1;
      end
      else begin
          $display("================== Halt! ==================");
          $finish;
      end
  end

endmodule