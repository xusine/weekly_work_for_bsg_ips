module bsg_carry_save_adder #(parameter width_p = "inv")(
   input [width_p-1:0] opA_i
  ,input [width_p-1:0] opB_i
  ,input [width_p-1:0] opC_i

  ,output [width_p-1:0] res_o
  ,output [width_p-1:0] car_o
);
  generate
    for(genvar i = 0; i < width_p; i++) begin : CSA_EACH
      assign res_o[i] = opA_i[i] ^ opB_i[i] ^ opC_i[i];
      assign car_o[i] = opA_i[i] & opB_i[i] | opA_i[i] & opC_i[i] | opB_i[i] & opC_i[i];
    end
  endgenerate
endmodule


