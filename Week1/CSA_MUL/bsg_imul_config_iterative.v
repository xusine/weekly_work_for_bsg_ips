module bsg_imul_config_iterative #(
  parameter width_p = 32
  ,parameter iter_step_p = 4
)
(
  input clk_i
  ,input rst_i
  ,input reset_when_done_i
  // shakehand signal
  ,input op_v_i
  ,output ready_o

  ,input [width_p-1:0] opA_i
  ,input opA_is_signed_i
  ,input [width_p-1:0] opB_i
  ,input opB_is_signed_i

  ,output [2*width_p-1:0] result_o 
  ,output result_v_o
  
);

  localparam integer iter_count_lp = (width_p - 1)/iter_step_p + 1;
  localparam cal_state_length_lp = `BSG_SAFE_CLOG2(iter_count_lp);

  localparam actual_width_lp = iter_count_lp * iter_step_p;
  

  wire self_is_ready = ready_o & op_v_i;
  // used for reset registers. combining the rst_i and reset_when_done_i
  wire reset_internal = rst_i | (reset_when_done_i & current_state_r == eDONE); 
  // counter used for jumping out of eCAL
  logic [cal_state_length_lp-1:0] cal_state_counter_r;
  // Counter update
  always_ff @(posedge clk_i) begin
    if(reset_internal) cal_state_counter_r <= '0;
    else cal_state_counter_r <= (current_state_r == eCAL) ? cal_state_counter_r + 1 : cal_state_counter_r;
  end

  // FSM
  typedef enum [2:0] {eIDNE, eNEG_A, eNEG_B, eCAL, eNEG_R_LOW, eNEG_R_HIGH, eDONE} state_e;
  state_e current_state_r, current_state_n;
  always_comb begin
    unique case(current_state_r)
      eIDNE : begin
        if(self_is_ready) 
            current_state_n = eNEG_A; 
        else 
          current_state_n = eIDNE;
      end
      eNEG_A : begin
        current_state_n = eNEG_B;
      end
      eNEG_B : begin
        current_state_n = eCAL;
      end
      eCAL : begin
        if(cal_state_counter_r == iter_count_lp - iter_count_lp'(1))
            current_state_n = eNEG_R_LOW;
        else
            current_state_n = eCAL;
      end
      eNEG_R_LOW : begin
        current_state_n = eNEG_R_HIGH;
      end
      eNEG_R_HIGH : begin
        current_state_n = eDONE;
      end
      eDONE:  current_state_n = eDONE;
      default : current_state_n = eIDNE;
    endcase
  end
  // update current_state_r
  assign ready_o = current_state_r == eIDNE;

  always_ff @(posedge clk_i) begin
    if(reset_internal) begin
      current_state_r <= eIDNE;
      $display("current state: %s",current_state_r.name());
    end
    else begin
      current_state_r <= current_state_n;
      $display("current state: %s",current_state_r.name());
    end
  end

  logic [actual_width_lp - 1:0] opA_r;
  logic [actual_width_lp - 1:0] opB_r;
  logic opA_neg_r; // used to record that opA is negative.
  logic opB_neg_r;
  // update input registers (opA, opB, opA_neg, opB_neg)
  always_ff @(posedge clk_i) begin
    if(reset_internal) begin
      opA_r <= '0;
      opB_r <= '0;
      opA_neg_r <= 1'b0;
      opB_neg_r <= 1'b0;
    end
    else begin
      unique case(current_state_r) 
        eIDNE: begin
          opA_r <= opA_i;
          opB_r <= opB_i;
          opA_neg_r <= opA_i[actual_width_lp-1] & opA_is_signed_i;
          opB_neg_r <= opB_i[actual_width_lp-1] & opB_is_signed_i;
        end
        eNEG_A: begin
          if(opA_neg_r) opA_r <= pca_opt[actual_width_lp-1:0]; else opA_r <= opA_r;
        end
        eNEG_B: begin
          if(opB_neg_r) opB_r <= pca_opt[actual_width_lp-1:0]; else opB_r <= opB_r;
        end
        eCAL : begin
          opB_r <= (opB_r >> iter_step_p);
        end
        default : begin
        end
      endcase
    end
  end

  logic [actual_width_lp:0] pca_opA;
  logic [actual_width_lp:0] pca_opB;

  wire [actual_width_lp+1:0] pca_opt = pca_opA + pca_opB; //PCA

  // configure PCA operands.
  always_comb begin
    unique case(current_state_r) 
      eIDNE: begin
        pca_opA = '0;
        pca_opB = '0;
      end
      eNEG_A: begin
        pca_opA = {1'b0,~opA_r};
        pca_opB = (actual_width_lp+1)'(1);
      end
      eNEG_B: begin
        pca_opA = {1'b0, ~opB_r};
        pca_opB = (actual_width_lp+1)'(1);
      end
      eCAL : begin
        pca_opA = csa_sum;
        pca_opB = csa_carry;
      end
      eNEG_R_LOW: begin
        pca_opA = {1'b0, ~result_r[actual_width_lp-1:0]};
        pca_opB = (actual_width_lp+1)'(1);
      end
      eNEG_R_HIGH: begin
        pca_opA = {1'b0, ~result_r[actual_width_lp+:actual_width_lp]};
        pca_opB = {{actual_width_lp{1'b0}}, carry_of_neg_res_r};
      end
      default: begin
        pca_opA = '0;
        pca_opB = '0;
      end
    endcase
  end

  // setup n-level CSA network.
  logic [actual_width_lp:0] csa_sum;
  logic [actual_width_lp:0] csa_carry;
  logic [iter_step_p-1:0] csa_out;

  generate begin: CSA_BLOCK
    if(iter_step_p == 1) begin
      assign csa_sum = {1'b0,opA_r & {actual_width_lp{opB_r[0]}}};
      assign csa_carry = result_remnant_r;
    end
    else begin 
`ifndef UNROLL 
      wire [actual_width_lp:0] sums [iter_step_p-2:0];
      wire [actual_width_lp:0] carrys [iter_step_p-2:0];
      // first CSA, which adds remnant, opA_r and opA_r << 1 together.
      bsg_carry_save_adder #(.width_p(actual_width_lp+1))
      first_csa(.opA_i(result_remnant_r)
        ,.opB_i({1'b0, opA_r & {actual_width_lp{opB_r[0]}}})
        ,.opC_i({opA_r & {actual_width_lp{opB_r[1]}}, 1'b0})
        ,.res_o(sums[0])
        ,.car_o(carrys[0])
      );
      assign csa_out[0] = sums[0][0];
      // Debug information output.
      always_ff @(posedge clk_i) begin
        if(current_state_r == eCAL) 
          $display("CSA%d : opA: %b, opB: %b, opC: %b, res: %b, car: %b"
          ,0,first_csa.opA_i,first_csa.opB_i,first_csa.opC_i,first_csa.res_o,first_csa.car_o
          );
      end
      // Other CSA
      for(genvar i = 1; i < iter_step_p - 1;i++) begin : CSA_STRING
          bsg_carry_save_adder #(.width_p(actual_width_lp + 1))
          csa(.opA_i({1'b0, sums[i-1][1+:actual_width_lp]})
            ,.opB_i(carrys[i-1])
            ,.opC_i({opA_r & {actual_width_lp{opB_r[i+1]}}, 1'b0})
            ,.res_o(sums[i])
            ,.car_o(carrys[i])
          );
          assign csa_out[i] = sums[i][0];
          // Debug information
          always_ff @(posedge clk_i) begin
            if(current_state_r == eCAL) 
            $display("CSA%d : opA: %b, opB: %b, opC: %b, res: %b, car: %b"
            ,i,csa.opA_i,csa.opB_i,csa.opC_i,csa.res_o,csa.car_o
            );
          end
      end
      assign csa_sum = {1'b0, sums[iter_step_p-2][actual_width_lp:1]};
      assign csa_carry = carrys[iter_step_p-2];
`else
      // Unrolling
      wire [actual_width_lp:0] sums_0;
      wire [actual_width_lp:0] carrys_0;
      bsg_carry_save_adder #(.width_p(actual_width_lp+1))
      first_csa(.opA_i(result_remnant_r)
        ,.opB_i({1'b0, opA_r & {actual_width_lp{opB_r[0]}}})
        ,.opC_i({opA_r & {actual_width_lp{opB_r[1]}}, 1'b0})
        ,.res_o(sums_0)
        ,.car_o(carrys_0)
      );
      assign csa_out[0] = sums_0[0];

      always_ff @(posedge clk_i) begin
        if(current_state_r == eCAL) 
          $display("CSA%d : opA: %b, opB: %b, opC: %b, res: %b, car: %b"
          ,0,first_csa.opA_i,first_csa.opB_i,first_csa.opC_i,first_csa.res_o,first_csa.car_o
          );
      end
      wire [actual_width_lp:0] sums_1;
      wire [actual_width_lp:0] carrys_1;

      bsg_carry_save_adder #(.width_p(actual_width_lp + 1))
      csa1(.opA_i({1'b0, sums_0[1+:actual_width_lp]})
        ,.opB_i(carrys_0)
        ,.opC_i({opA_r & {actual_width_lp{opB_r[2]}}, 1'b0})
        ,.res_o(sums_1)
        ,.car_o(carrys_1)
      );
      assign csa_out[1] = sums_1[0];
      always_ff @(posedge clk_i) begin
        if(current_state_r == eCAL) 
        $display("CSA%d : opA: %b, opB: %b, opC: %b, res: %b, car: %b"
        ,1,csa1.opA_i,csa1.opB_i,csa1.opC_i,csa1.res_o,csa1.car_o
        );
      end
       
      wire [actual_width_lp:0] sums_2;
      wire [actual_width_lp:0] carrys_2;

      bsg_carry_save_adder #(.width_p(actual_width_lp + 1))
      csa2(.opA_i({1'b0, sums_1[1+:actual_width_lp]})
        ,.opB_i(carrys_1)
        ,.opC_i({opA_r & {actual_width_lp{opB_r[3]}}, 1'b0})
        ,.res_o(sums_2)
        ,.car_o(carrys_2)
      );
      assign csa_out[2] = sums_2[0];
      always_ff @(posedge clk_i) begin
        if(current_state_r == eCAL) 
        $display("CSA%d : opA: %b, opB: %b, opC: %b, res: %b, car: %b"
        ,2,csa2.opA_i,csa2.opB_i,csa2.opC_i,csa2.res_o,csa2.car_o
        );
      end

      assign csa_sum = {1'b0, sums_2[actual_width_lp:1]};
      assign csa_carry = carrys_2;
`endif
    end
  end
  endgenerate

  assign csa_out[iter_step_p-1] = pca_opt[0];

  wire result_is_negative = opA_neg_r ^ opB_neg_r;

  reg [2*actual_width_lp-1:0] result_r;
  logic [actual_width_lp:0] result_remnant_r; // the bits need to be put into next turn
  logic carry_of_neg_res_r; // the carry by making lower bit negative.

  // update of result_r, result_remnant_r and carry_of_neg_res_r.
  always_ff @(posedge clk_i) begin
    if(reset_internal) begin
      result_r <= '0;
      result_remnant_r <= '0;
      carry_of_neg_res_r <= 1'b0;
      result_r <= '0;
    end
    else begin
        unique case(current_state_r) 
          eCAL: begin
            result_remnant_r <= {1'b0,pca_opt[1+:actual_width_lp]};
            result_r[actual_width_lp-1:0] <= {csa_out,result_r[actual_width_lp-1:iter_step_p]};
            carry_of_neg_res_r <= 1'b0;
            if(cal_state_counter_r == iter_count_lp - iter_count_lp'(1)) begin
              result_r <= {pca_opt[1+:actual_width_lp],{csa_out,result_r[actual_width_lp-1:iter_step_p]}};
            end
            // Debug
            $display("PCA: X(SUM)=%b, Y(CARRY)=%b, Z=%b",csa_sum,csa_carry,pca_opt);
            $display("CSA_OUT:%b",csa_out);
            $display("result_remnant_r:%b",pca_opt[1+:actual_width_lp]);
          end
          eNEG_R_LOW: begin // making low part negative
            result_remnant_r <= '0;
            if(result_is_negative) begin
              result_r[0+:actual_width_lp] <= pca_opt[actual_width_lp-1:0];
              carry_of_neg_res_r <= pca_opt[actual_width_lp];
            end
            else begin
              carry_of_neg_res_r <= 1'b0;
            end
          end
          eNEG_R_HIGH: begin // for high part
            result_remnant_r <= '0;
            if(result_is_negative) begin
              result_r[actual_width_lp+:actual_width_lp] <= pca_opt[actual_width_lp-1:0];
              carry_of_neg_res_r <= pca_opt[actual_width_lp];
            end
            else begin
              carry_of_neg_res_r <= 1'b0;
            end
          end
          default: begin
            result_remnant_r <= '0;
            carry_of_neg_res_r <= 1'b0;
          end
        endcase
    end
  end
  
  assign result_v_o = current_state_r == eDONE;
  assign result_o = result_r[2*width_p-1:0];
endmodule