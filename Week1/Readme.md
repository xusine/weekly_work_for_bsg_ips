## Week1

Mar. 4 - Mar. 10
By Shanqing Lin, Fudan University

#### Busy Beaver (Busy_Beaver)

This is a SystemVerilog project used to get familiar with rules by BaseJump.

#### CSA String Multiplier (CSA_MUL)

A basic implementation of iterative multiplier with configurable stride, using a string of full adders (carry save adders)
