## Weekly Report for BaseJump STL
Mar.18 - Mar.24
by Shanqing Lin, Fudan University

#### Plan

1. Optimize the multiplier by merging the conditions of signed and unsigned number.
2. Build a nonrestoring divider.
3. Read *Computer Arithmetic Algorithms and Hardware Design* to learn how to build high-radix divider.

#### Task on Thursday

1. Keep all intermediate results redundant, including sums of partial product, which means no carry propagate addition is needed during calculation of partial products.
2. Add a new stage for CPA to recover the final result.
3. Make an assertion to check whether the stride of iteration is aligned with `width_p` which should be octave. 

#### Task on Friday

1. Figure out the structure of iterative divider built by Shaolin Xie.
2. Design the nonresotring divider capable of dealing with both signed and unsigned number, where 64bit/32bit unsigned numbers are extended to 65bit/33bit signed numbers. Here is the picture of my design:

![](bsg_idiv_iterative.svg)

Details can be found here: *Computer Arithmetic Algorithms and Hardware Design*, 2nd, Section 13.4

3. Implement the divider using SystemVerilog. 

#### Problems on Friday

I'm not sure how to cope with overflow and division by zero. However, RISC-V defines the semantics of division by zero and division overflow which I think can be applied after high-radix divider is finished.

#### Summary on Friday

When I learned x86 ISA, I couldn't understand why the result of division is placed in this way that EDX is the remainder while quotient is put in EAX. Now designing a similar divider gives me a better understanding of the reason.

#### Task on Saturday

1. Write a C++ testbench to verify the nonrestoring divider. A C++ testbench template for Verilator is written for convenience as well.

2. Learn how to build high-radix divider.

#### Problems on Saturday

I don't know how to change the include macro in C/C++ depending on the project name. For instance, A C/C++ testbench for a module named `bsg_mul_iterative` includes a head file where the module compiled by Verilator is declared:

```cpp
#include<bsg_mul_iterative/Vbsg_mul_iterative.h>
```

Similar arguments of g++ like -DMACRO[=VALUE] work for Verilator, so passing on the module name by macro definition is a good solution except I don't know how to coordinate #include and macros.

Another solution is to generate a head file in Makefile, but it's not so clear. I'm searching for other method.

#### Summary on Saturday

Division is more complicated than multiplication and intricate optimization for division is challenging. 

#### Task on Sunday

1. Merge two versions of multiplier.
2. Extend the data width of Wallace tree adder so that the extended sign bits are taken into account when calculation, indicating the conversions from negative number to positive one can be displaced. 
3. Pass the determined result bits of CSA to CPA. Shrink the result registers and enhancing the efficiency of CPA, whose size is also scaled down (32bit at present).

#### Problems on Sunday

None.

#### Summary on Sunday

I think I still have potential to improve my design and I put my plan in TODO list, hoping to solve them in the future.

#### Plan Next Week

1. Figure out the structure of high-radix divider.
2. Better the multiplier on area and cost.
3. Combining the multiplier and divider together, which can be difficult.

#### Wishlist

1. A chance to go over the book *Computer Arithmetic Algorithms and Hardware Design*. There are abundant techniques to polish ip cores in STL at present.

2. A opportunity to build a small module concerning RISC-V. 