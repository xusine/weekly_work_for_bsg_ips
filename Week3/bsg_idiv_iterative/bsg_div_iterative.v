/*************************************************
==============================
bsg_div_iterative.v
3/23/2019 sqlin16@fudan.edu.cn
=============================

An iterative unrestored divider for unsiged and signed number, with iterative step equal to 1.

STATE:
eIDNE: Wait for operands(dividend and divisor)
eOV: Do the first iteration and check whether the quotient will be overflow.
eCAL: Iterate to calculate each bit of quotient
eADJREM: Match the sign of remainder same with dividend's.
eADJQUO: Adjust the quotient relatively.
eDONE: Done.

TODO:
  1. Build a SRT divider.
  2. Build a high radix divider.
  3. Add logic to detect the unnecessary iteration.
  4. Add a cache to store lasest (operand,result) pairs.

**************************************************/

module bsg_div_iterative #(parameter width_p = 32)(

  input clk_i
  ,input rst_i
  // dividend 
  ,input [2*width_p-1:0] dividend_i
  ,input dividend_is_signed_i
  // divisor
  ,input [width_p-1:0] divisor_i
  ,input divisor_is_signed_i

  // input shakehand signal
  ,input v_i
  ,output ready_o
  ,input reset_when_done_i

  // output
  ,output [width_p-1:0] quotient_o
  ,output [width_p-1:0] remainder_o
  // output shakehand signal
  ,output v_o
  // state output, which should connect to the interrupt/exception handle unit.
  ,output is_overflow_o
  ,output divisor_is_zero_o

);

localparam cal_counter_size_lp = `BSG_SAFE_CLOG2(width_p); // determine how many iterations will be done.

typedef enum [2:0] {eIDNE, eOV, eCAL, eADJREM, eADJQUO, eDONE, eERROR} state_e;
state_e current_state_r, current_state_n;

logic is_overflow_n;

logic need_adjust; // whether the final remainder should be modified.

wire reset_internal = rst_i | (current_state_r == eDONE && reset_when_done_i);

assign divisor_is_zero_o = divisor_r == '0;

always_comb begin // update of current_state_n
  unique case(current_state_r) 
    eIDNE: begin
      if(v_i)
        current_state_n = divisor_i == '0  ? eERROR : eOV;
      else
        current_state_n = eIDNE;
    end
    eOV: begin
      current_state_n = is_overflow_n ? eERROR : eCAL;
    end
    eCAL: begin
      if(cal_counter_r == '1) begin
        current_state_n = need_adjust ? eADJREM : eDONE;
      end
      else
        current_state_n = eCAL;
    end
    eADJREM: 
      current_state_n = eADJQUO;
    eADJQUO:
      current_state_n = eDONE;
    eDONE:
      current_state_n = eDONE;
    eERROR:
      current_state_n = eERROR;
    default:
      current_state_n = eIDNE;
  endcase
end
// update FSM.
always_ff @(posedge clk_i) begin
  if(reset_internal) 
    current_state_r <= eIDNE;
  else
    current_state_r <= current_state_n;
end

reg [cal_counter_size_lp-1:0] cal_counter_r;
// update cal_counter_r
always_ff @(posedge clk_i) begin
  if(reset_internal)
    cal_counter_r <= '0;
  else 
  unique case(current_state_r)
    eCAL: begin
      cal_counter_r <= cal_counter_r + 1;
    end
    eOV: begin
      cal_counter_r <= cal_counter_r + 1;
    end
    default: begin
    end
  endcase
end

reg [2*width_p:0] dividend_r;
reg [width_p:0] divisor_r;
reg dividend_sign_r;
reg is_overflow_r;

wire dividend_sign_r_n = dividend_is_signed_i & dividend_i[2*width_p-1];
wire divisor_sign = divisor_is_signed_i & divisor_i[width_p-1];

assign is_overflow_n = dividend_sign_r == cpa_res[width_p];
assign is_overflow_o = is_overflow_r;

wire z_d_same_sign = dividend_r[2*width_p] == divisor_r[width_p]; // subtraction if equal

assign need_adjust = cpa_res[width_p] != dividend_sign_r && cpa_res[width_p-1:0] != '0; // We need to adjust the result if the sign of remainder doesn't match the sign of dividend.

always_ff @(posedge clk_i) begin
  if(reset_internal) begin
    dividend_r <= '0;
    divisor_r <= '0;
    dividend_sign_r <= 1'b0;
    is_overflow_r <= 1'b0;
  end
  else 
    unique case(current_state_r)
      eIDNE: begin
        if(v_i) begin
          dividend_r <= {dividend_sign_r_n,dividend_i};
          dividend_sign_r <= dividend_sign_r_n;
          divisor_r <= {divisor_sign,divisor_i};
          is_overflow_r <= 1'b0;
        end
      end
      eOV: begin
        is_overflow_r <= is_overflow_n;
        dividend_r <= {cpa_res,dividend_r[width_p-2:0],z_d_same_sign};
      end
      eCAL: begin
        is_overflow_r <= 1'b0;
        dividend_r <= {cpa_res,dividend_r[width_p-3:0],z_d_same_sign}; // conversion from boost recoding to actual recoding.
      end
      eADJREM: begin
        is_overflow_r <= 1'b0;
        dividend_r[2*width_p:width_p] <= cpa_res;
      end
      eADJQUO: begin
        is_overflow_r <= 1'b0;
        dividend_r[width_p-1:0] <= cpa_res[width_p-1:0];
      end
      eERROR: begin
      end
      default: begin
        is_overflow_r <= 1'b0;
      end
    endcase
end

// carry propagate adder
logic [width_p:0] cpa_opA;
logic [width_p:0] cpa_opB;
logic is_sub;

wire [width_p:0] cpa_res = cpa_opA + cpa_opB + is_sub;


// if dividend is positive, an absolute value of divisor is added to remainder.
wire adjust_is_sub = dividend_sign_r ^ divisor_r[width_p]; 

//TODO: find out the sign of operand B.
always_comb begin // update adder operands
  unique case(current_state_r)
    eOV: begin
      cpa_opA = dividend_r[2*width_p-1-:width_p+1];
      cpa_opB = divisor_r ^ {(width_p+1){z_d_same_sign}};
      is_sub = z_d_same_sign; 
    end
    eCAL: begin
      cpa_opA = dividend_r[2*width_p-1-:width_p+1];
      cpa_opB = divisor_r ^ {(width_p+1){z_d_same_sign}};
      is_sub = z_d_same_sign;
    end
    eADJREM: begin
      cpa_opA = dividend_r[2*width_p:width_p];
      cpa_opB = divisor_r ^ {(width_p + 1){adjust_is_sub}};
      is_sub = adjust_is_sub;
    end
    eADJQUO: begin
      cpa_opA = dividend_r[width_p-1:0]; // q
      cpa_opB = (width_p + 1)'(1) ^ {(width_p + 1){~adjust_is_sub}};
      is_sub = ~adjust_is_sub;
    end
    default: begin
      cpa_opA = '0;
      cpa_opB = '0;
      is_sub = 1'b0;
    end
  endcase
end

assign v_o = current_state_r == eDONE;
assign ready_o = current_state_r == eIDNE;
assign quotient_o = dividend_r[width_p-1:0];
assign remainder_o = dividend_r[2*width_p-1:width_p];

endmodule