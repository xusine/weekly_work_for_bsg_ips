/***************** TEST RATIONALE *************************
1. STATEB SPACE
  Random input for signed operands.
2. PARAMETERIZATION
  width_p = 32
  
*********************************************************/

#include "verilated.h"
#include "bsg_div_iterative/Vbsg_div_iterative.h"

#include<stdio.h>
#include<stdlib.h>
#include<time.h>
#include<inttypes.h>

int main(int argc, char** argv){
    Verilated::commandArgs(argc,argv);
    srand(time(NULL));

    Vbsg_div_iterative *dut = new Vbsg_div_iterative;

    // initial begin
    dut->clk_i = 0;
    dut->rst_i = 0;
    dut->reset_when_done_i = 1;
    dut->dividend_i = 0;
    dut->dividend_is_signed_i = 1;
    dut->divisor_i = 0;
    dut->divisor_is_signed_i = 1;
    dut->v_i = 0;
    dut->eval();

    dut->rst_i = 1;
    dut->clk_i = 1;
    dut->eval();
    dut->clk_i = 0;
    dut->eval();
    dut->clk_i = 1;
    dut->rst_i = 0;
    dut->eval();

    dut->clk_i = 0;
    dut->eval();

    
    dut->dividend_i = rand();
    dut->divisor_i = rand();
    dut->v_i = 1;

    int turn = 0;

    while(dut->v_o == 0){
      dut->clk_i = 1;
      dut->eval();
      dut->clk_i = 0;
      dut->eval();
      ++turn;
      printf("%d:\n",turn);
      if(dut->is_overflow_o){
        printf("Overflow!\n");
        break;
      }
        
      if(dut->divisor_is_zero_o){
        printf("Divisor is zero!\n");
        break;
      }
        
    }
    

    printf("%d: %d,%d | %d, %d\n",turn,dut->quotient_o,dut->remainder_o,dut->dividend_i/dut->divisor_i,dut->dividend_i % dut->divisor_i);

    return 0;
}
