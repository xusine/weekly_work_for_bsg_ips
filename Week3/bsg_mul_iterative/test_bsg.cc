/***************** TEST RATIONALE *************************
1. STATEB SPACE
  Random input for unsigned and signed operands.
2. PARAMETERIZATION
  width_p = 32, iter_step_p = 4/8/16
  
*********************************************************/

#include "verilated.h"

#include "bsg_test_template.hpp"

#include "bsg_mul_iterative/Vbsg_mul_iterative.h"

#include<stdio.h>
#include<stdlib.h>
#include<time.h>
#include<inttypes.h>

int main(int argc, char** argv){
    Verilated::commandArgs(argc,argv);
    srand(time(NULL));

    BaseJump::TestWrapper<Vbsg_mul_iterative> wrapper;

    auto dut = wrapper.dut();
    // initial begin
    dut->clk_i = 0;
    dut->rst_i = 0;
    dut->reset_when_done_i = 1;
    dut->op_v_i = 0;
    dut->opA_i = 0;
    dut->opA_is_signed_i = 0;
    dut->opB_i = 0;
    dut->opB_is_signed_i = 0;
    // end
    dut->eval();
    // Reset

    wrapper.reset();
    // unsigned value

    printf("============Testing unsigned number!===============\n");

    for (int i = 0; i < 10; ++i) {

        unsigned int opA = rand() & 0xFF;
        unsigned int opB = rand() & 0xFF;
        wrapper.setCycleNumber();
        dut->opA_i = opA;
        dut->opB_i = opB;
        dut->op_v_i = 1;
        int turn = 0;
        while(dut->result_v_o == 0){
            wrapper.tick();
        }

        uint64_t res = dut->result_o;
        uint64_t cpp_res = uint64_t(opA)*opB;

        printf("Afer %llu, %lluD * %lluD, Output: %llxH, In C++: %llxH, difference: %llxH\n",turn,opA,opB,res, cpp_res, res - cpp_res);
        //printf("Afer %u, %u * %u, Output: %x, In C++: %x, difference: %u\n",turn,opA,opB,res, cpp_res, res - cpp_res);
        wrapper.tick();
    }

    dut->opA_is_signed_i = 1;
    dut->opB_is_signed_i = 1;

    printf("============Testing signed number!===============\n");

    for (int i = 0; i < 10; ++i) {

        int64_t opA = -rand();
        int64_t opB = rand();

        wrapper.setCycleNumber();
        dut->opA_i = opA;
        dut->opB_i = opB;
        dut->op_v_i = 1;
        int turn = 0;
        while(dut->result_v_o == 0){
            wrapper.tick();
            ++turn;
        }

        int64_t res = dut->result_o;
        int64_t cpp_res = int64_t(opA)*opB;

        printf("Afer %lld, %lld * %lld, Output: %llxH, In C++: %llxH, difference: %llxH\n",turn,opA, opB,res , cpp_res, res - cpp_res);
        wrapper.tick();
    }

    return 0;
}
